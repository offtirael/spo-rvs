<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
    </head>
    <body>
        <%@page import="javax.naming.*, paymentsystem.*" %>
        <%@ page errorPage="error.jsp"%>
        <%
            request.setCharacterEncoding("UTF-8");
            PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
            String enc = request.getCharacterEncoding();
            
            ejbRef.addNewUser(ejbRef.convertStringToUTF(enc, request.getParameter("username")),
                    ejbRef.convertStringToUTF(enc, request.getParameter("password")),
                    ejbRef.convertStringToUTF(enc, request.getParameter("firstName")),
                    ejbRef.convertStringToUTF(enc, request.getParameter("lastName")));
            ejbRef.login(request.getParameter("username"), request.getParameter("password"));
            response.sendRedirect("user.jsp");
        %>
    </body>
</html>