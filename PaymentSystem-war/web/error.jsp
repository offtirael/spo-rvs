<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isErrorPage="true" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <h1>Обнаружена ошибка</h1>
        <p style="color: red">${pageContext.errorData.throwable.message}</p>
        <p style="color: red">
            ${pageContext.errorData.throwable.printStackTrace()}
        </p>
        <p><a href="index.jsp">Return</a></p>
    </body>
</html>