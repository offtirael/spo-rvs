<%-- 
    Document   : logout
    Created on : 12.11.2013, 16:16:09
    Author     : tirael
--%>

<%@page import="paymentsystem.Users"%>
<%@page import="paymentsystem.PaymentSystemSessionRemote"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <%
            PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
            Users usr = ejbRef.getCurrentUser();
            if (usr == null) {
                response.sendRedirect("index.jsp");
            } else {
                ejbRef.logout();
                response.sendRedirect("index.jsp");
            }
        %>
    </body>
</html>
