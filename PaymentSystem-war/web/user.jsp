<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <div class="container">
        <%@page import="javax.naming.*, paymentsystem.*" %>
        <%@ page errorPage="error.jsp"%>
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <%
            PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
            Users usr = ejbRef.getCurrentUser();
            if (usr == null) {
                response.sendRedirect("index.jsp");
            }
        %>

        <p class="lead">Здравствуйте, <%= usr.getFirstName()%> <%= usr.getLastName()%> | <a href="logout.jsp">Выход</a></p>
        <%
            List<Account> accounts = ejbRef.getUserAccounts(usr);
            if (accounts == null) {
        %>
        <p class="lead">У вас ещё нет счетов.</p> 
        <% } else {%>
        <p class="lead">Ваши счета:</p>
        <table class="table table-hover">
            <tr>
                <th>№ счёта</th>
                <th>Остаток</th>
            </tr>
            <%
                for (Account acc : accounts) {
            %>
            <tr>
                <td><a href="account.jsp?accId=<%=acc.getId()%>"><%=acc.getId()%></a></td>
                <td><%=acc.getBalance()%></td>
            </tr>
            <%
                }
            %>
        </table>
        <% }%>
        <p class="lead"><a href="addAccount.jsp">Создать счёт</a></p>
        </div>
    </body>
</html>