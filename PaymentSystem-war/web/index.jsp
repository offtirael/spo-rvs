<%@page import="paymentsystem.Users"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="paymentsystem.PaymentSystemSessionRemote"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <div class="container">
        <%!            
            PaymentSystemSessionRemote ejbRef;
        %>
        <%            
            InitialContext ic = new InitialContext();
            ejbRef = (PaymentSystemSessionRemote) ic.lookup("paymentsystem.PaymentSystemSessionRemote");
            session.setAttribute("ejbRef", ejbRef);
            
            Users usr = ejbRef.getCurrentUser();
            if (usr != null) {
                response.sendRedirect("user.jsp");
            }
        %>
        <form class="form-horizontal" action="doLogin.jsp" method="POST">
            <legend>Welcome to PaymentSystem</legend>
            <fieldset>
            <div class="control-group">
                <label class="control-label" for="username">Логин</label>

                <div class="controls">
                    <input type="text" name="username" value=""/>
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label" for="password">Пароль</label>

                <div class="controls">
                    <input type="password" name="password" value="" />
                </div>
            </div>
            
            <div class="control-group">
                <label class="control-label" for="submit"></label>

                <div class="controls">
                    <button id="submit" name="submit" class="btn btn-success">Войти</button>
                </div>
            </div>
            </fieldset>
        </form>
        <p class="lead"><a href="addUser.jsp">Добавить нового пользователя</a></p>
        </div>
    </body>
</html>