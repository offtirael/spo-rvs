<%@page import="paymentsystem.Users"%>
<%@page import="paymentsystem.PaymentSystemSessionRemote"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <<div class="container">
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <%            
            PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
            Users usr = ejbRef.getCurrentUser();
            if (usr != null) {
                response.sendRedirect("user.jsp");
            }
        %>
        <h1>Войти</h1>
        <form action="doLogin.jsp" method="POST">
            <p>Логин: <input type="text" name="username" value=""/></p>
            <p>Пароль: <input type="password" name="password" value="" /></p>
            <input type="submit" value="Submit" />
        </form>
        </div>
    </body>
</html>