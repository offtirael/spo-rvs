<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <%@page import="javax.naming.*, paymentsystem.*" %>
        <%@ page errorPage="error.jsp"%>
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <%
            PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
            Users usr = ejbRef.getCurrentUser();
            if (usr == null) {
                response.sendRedirect("index.jsp");
            } else {
                String enc = request.getCharacterEncoding();
                int source_acc_id = (int) ejbRef.getCurrentAccount().getId();
                int dest_acc_id = Integer.parseInt(request.getParameter("destinationAccId"));

                try {
                    if (source_acc_id == dest_acc_id) {
                        ejbRef.addNewPayment("Пополнение счёта",
                                Integer.parseInt(request.getParameter("amount")),
                                source_acc_id, dest_acc_id);
                    } else {
                        ejbRef.addNewPayment(ejbRef.convertStringToUTF(enc, request.getParameter("purpose")),
                                Integer.parseInt(request.getParameter("amount")),
                                source_acc_id, dest_acc_id);
                    }
                    response.sendRedirect("account.jsp");
                }
                catch (IllegalOperationException e){%>
                <p class="lead">Ошибка!</p>
                <p class="lead"><%=e.getMessage()%></p>
                <p class="lead"><a href="account.jsp">Назад</a></p>
                <%
                }
            }

        %>


    </body>
</html>