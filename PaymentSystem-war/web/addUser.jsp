<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <%
            request.setCharacterEncoding("UTF-8");
        %>
        <div class="container">
        <form class="form-horizontal" action="doAddUser.jsp" method="POST">
            <legend>Создать нового пользователя</legend>
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="username">Логин</label>

                    <div class="controls">
                        <input type="text" name="username" value=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="password">Пароль</label>

                    <div class="controls">
                        <input type="password" name="password" value="" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="firstName">Имя</label>

                    <div class="controls">
                        <input type="text" name="firstName" value=""/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="lastName">Имя</label>

                    <div class="controls">
                        <input type="text" name="lastName" value="" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="submit"></label>

                    <div class="controls">
                        <button id="submit" name="submit" class="btn btn-success">Регистрация</button>
                    </div>
                </div>
            </fieldset>
        </form>
        </div>
    </body>
</html>