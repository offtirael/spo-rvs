<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <div class="container">
            <%@page import="javax.naming.*, paymentsystem.*" %>
            <%@ page errorPage="error.jsp"%>
            <%                
                request.setCharacterEncoding("UTF-8");
            %>
            <%                
                PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
                Users usr = ejbRef.getCurrentUser();
                if (usr == null) {
                    response.sendRedirect("index.jsp");
                }
            %>

            <%                
                String accId = request.getParameter("accId");
                if (accId != null) {
                    try {
                        ejbRef.setCurrentAccount(Integer.parseInt(accId));
                    } catch (IllegalOperationException e) {
                        response.sendRedirect("account.jsp");
                    }
                }
            %>
            <p class="lead">Ваши операции по счёту №<%=ejbRef.getCurrentAccount().getId()%></p>
            <%                
                List<Payment> history = ejbRef.getAccountHistory();
                if (history == null) {
            %>
            <p class="lead">Вы не совершили ни одного платежа</p>

            <% } else {%>
            <table class="table table-hover">
                <tr>
                    <th>№ счёта отправителя</th>
                    <th>№ счёта получателя</th>
                    <th>Сумма</th>
                    <th>Цель</th> 
                    <th>Дата</th>
                </tr>
                <%                    
                    for (Payment pmnt : history) {
                %>
                <tr>
                    <td><%=pmnt.getSourceAccId().getId()%></td>
                    <td><%=pmnt.getDestinationAccId().getId()%></td>
                    <td><%=pmnt.getAmount()%></td>
                    <td><%=pmnt.getPurpose()%></td>
                    <td><%=pmnt.getPaymentDate()%></td>
                </tr>
                <%}%>
            </table>
            <%}%>
            <p class="lead"><a href="addPayment.jsp">Создать платёж</a></p>
            <p class="lead"><a href="fundAccount.jsp">Пополнить счёт</a></p>
            <p class="lead"><a href="user.jsp">Назад</a></p>
        </div>
    </body>
</html>