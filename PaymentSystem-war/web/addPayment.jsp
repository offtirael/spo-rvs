<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <div class="container">
            <%@page import="javax.naming.*, paymentsystem.*" %>
            <%
            request.setCharacterEncoding("UTF-8");
            %>
            <%
                PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
                Users usr = ejbRef.getCurrentUser();
                if (usr == null) {
                    response.sendRedirect("index.jsp");
                }%>

            <form class="form-horizontal" action="doAddPayment.jsp" method="POST">
                <legend>Создать новый платёж</legend>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="destinationAccId">Номер счёта получателя</label>

                        <div class="controls">
                            <input type="text" name="destinationAccId" value="" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="amount">Сумма</label>

                        <div class="controls">
                            <input type="text" name="amount" value=""/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="purpose">Цель</label>

                        <div class="controls">
                            <input type="text" name="purpose" value="" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="submit"></label>

                        <div class="controls">
                            <button id="submit" name="submit" class="btn btn-success">Создать</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>