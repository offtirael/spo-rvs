<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>PaymentSystem Web Application</title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap.css" />
    </head>
    <body>
        <div class="container">
            <%@page import="javax.naming.*, paymentsystem.*" %>
            <%
            request.setCharacterEncoding("UTF-8");
            %>
            <%
                PaymentSystemSessionRemote ejbRef = (PaymentSystemSessionRemote) session.getAttribute("ejbRef");
                Users usr = ejbRef.getCurrentUser();
                if (usr == null) {
                    response.sendRedirect("index.jsp");
                }%>

            <form class="form-horizontal" action="doAddPayment.jsp" method="POST">
                <legend>Пополнить счёт №<%=ejbRef.getCurrentAccount().getId()%></legend>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="amount">Сумма</label>

                        <div class="controls">
                            <input type="text" name="amount" value=""/>
                        </div>
                    </div>
                    <input type="hidden" name="destinationAccId" value="<%=ejbRef.getCurrentAccount().getId()%>">

                    <div class="control-group">
                        <label class="control-label" for="submit"></label>

                        <div class="controls">
                            <button id="submit" name="submit" class="btn btn-success">Пополнить</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>