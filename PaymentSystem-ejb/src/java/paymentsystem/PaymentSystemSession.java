/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paymentsystem;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Date;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author tirael
 */
@Stateful
public class PaymentSystemSession implements PaymentSystemSessionRemote, PaymentSystemSessionLocal {

    @PersistenceContext(unitName = "PaymentSystem-ejbPU")
    private EntityManager em;
    private Users current_user = null;
    private Account current_account = null;

    /*
     * TODO:
     * - Добавить тест текущего юзера в платежи
     */
    @Override
    public void addNewUser(String username, String password, String firstName, String lastName) {
        Users user = new Users();
        user.setUsername(username);
        user.setPassw(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        em.persist(user);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public void persist(Object object) {
        em.persist(object);
    }

    @Override
    public void addNewAccount(int balance, Users user) {
        Account acc = new Account();
        acc.setBalance(balance);
        acc.setUserId(user);
        acc.setCreatedAt(new Date());
        em.persist(acc);
    }

    @Override
    public void addNewPayment(String purpose, int amount, int source_acc_id, int destination_acc_id)
            throws IllegalOperationException {
        
        Query q1 = em.createNamedQuery("Account.findById");
        q1.setParameter("id", source_acc_id);
        List<Account> res_lst1 = q1.getResultList();
        if (res_lst1.isEmpty()){
            throw new IllegalOperationException("Счёт №" + source_acc_id + " отсутствует");
        }
        Account source_acc = (Account) res_lst1.get(0);
        
        if(!source_acc.getUserId().equals(current_user)){
            throw new IllegalOperationException("Нельзя проводить операции над чужими счетами");
        }
        
        
        q1.setParameter("id", destination_acc_id);
        List<Account> res_lst2 = q1.getResultList();
        if (res_lst2.isEmpty()){
            throw new IllegalOperationException("Счёт №" + destination_acc_id + " отсутствует");
        }
        Account destination_acc = (Account) res_lst2.get(0);
        
        if (source_acc.equals(destination_acc)) {
            source_acc.setBalance(source_acc.getBalance() + amount);
        } else {
            
            int balance = source_acc.getBalance();
            if (balance < amount) {
                throw new IllegalOperationException("Недостаточно денег на счёте для этой операции");
            } else {
                source_acc.setBalance(source_acc.getBalance() - amount);
                destination_acc.setBalance(destination_acc.getBalance() + amount);
            }
        }
        
        Payment pt = new Payment();
        pt.setPurpose(purpose);
        pt.setAmount(amount);
        pt.setPaymentDate(new Date());
        pt.setSourceAccId(source_acc);
        pt.setDestinationAccId(destination_acc);
        em.persist(pt);

        

    }

    @Override
    public List<Payment> getAccountHistory() {
        Query query = em.createNamedQuery("Payment.findByAccount");
        query.setParameter("accID", current_account);
        List<Payment> payments = query.getResultList();

        if (payments.size() > 0) {
            return payments;
        } else {
            return null;
        }
    }

    /*
     * TODO:
     * - implement login, logout methods
     * - Web-interface(JSP)
     */
    @Override
    public void login(String username, String passw) {
        Query query = em.createNamedQuery("Users.findByUsername");
        query.setParameter("username", username);
        List users = query.getResultList();

        if (users.size() > 0) {
            Users user = (Users) users.get(0);
            if (passw.equals(user.getPassw())) {
                current_user = user;
            } else {
                current_user = null;
            }
        } else {
            current_user = null;
        }
    }

    @Override
    public Users getCurrentUser() {
        return current_user;
    }

    @Override
    public List<Account> getUserAccounts(Users user) {
        Query query = em.createNamedQuery("Account.findByUser");
        query.setParameter("user", user);
        List<Account> accounts = query.getResultList();

        if (accounts.size() > 0) {
            return accounts;
        } else {
            return null;
        }
    }

    @Override
    public void setCurrentAccount(int accId) throws IllegalOperationException {
        Query q1 = em.createNamedQuery("Account.findById");
        q1.setParameter("id", new Integer(accId));
        Account account = (Account) q1.getSingleResult();
        
        if(!account.getUserId().equals(current_user)){
            throw new IllegalOperationException("Доступ к чужим счетам запрещен");
        }
        
        current_account = account;
    }

    @Override
    public Account getCurrentAccount() {
        return current_account;
    }

    @Remove
    @Override
    public void logout() {
        current_account = null;
        current_user = null;
    }

    @Override
    public String convertStringToUTF(String characterEncoding, String paramValue) {
        if (paramValue != null) {
            if (characterEncoding == null) {
                characterEncoding = "8859_1";
            }
            try {
                paramValue = new String(paramValue.getBytes(characterEncoding), "UTF8");
            } catch (UnsupportedEncodingException e) {
            }
        }
        return paramValue;
    }
}
