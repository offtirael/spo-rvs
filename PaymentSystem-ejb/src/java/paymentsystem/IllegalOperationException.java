/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paymentsystem;

/**
 *
 * @author tirael
 */
public class IllegalOperationException extends Exception {

    /**
     * Creates a new instance of
     * <code>IllegalOperation</code> without detail message.
     */
    public IllegalOperationException() {
    }

    /**
     * Constructs an instance of
     * <code>IllegalOperation</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public IllegalOperationException(String msg) {
        super(msg);
    }
}
