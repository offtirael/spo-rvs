/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paymentsystem;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author tirael
 */
@Remote
public interface PaymentSystemSessionRemote {

    void addNewUser(String username, String password, String firstName, String lastName);

    void addNewAccount(int balance, Users user);

    void addNewPayment(String purpose, int amount, int source_acc, int destination_acc)
            throws IllegalOperationException;
    
    List<Payment> getAccountHistory();

    void login(String username, String passw);

    Users getCurrentUser();

    List<Account> getUserAccounts(Users user);

    void setCurrentAccount(int accId) throws IllegalOperationException;

    Account getCurrentAccount();

    void logout();
    
    String convertStringToUTF(String characterEncoding, String paramValue);
    
}
